<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 17.07.17
 */
return [
    /**
     * название класса-провайдера
     * @see SuiteSoft\LaravelSms\Providers
     */
    'provider' => env('SMS_PROVIDER', \SuiteSoft\LaravelSms\Providers\SmsRuAdapter::class),

    /**
     * настройки, специфичные для провайдера
     */
    'provider_options' => [
        'login' => env('SMS_LOGIN'),
        'password' => env('SMS_PASSWORD'),
    ],
];
