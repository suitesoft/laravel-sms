<?php

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Created by PhpStorm.
 * User: seminsergey
 * Date: 06.08.2018
 * Time: 21:16
 */
class SmsSendTest extends TestCase
{
    /**
     *
     */
    public function testSendSms()
    {

        $text = '';
        $logger = new Class($text) implements LoggerInterface
        {

            private $_text;

            /**
             *  constructor.
             * @param $text
             */
            public function __construct($text)
            {
                $this->_text = $text;
            }

            /**
             * System is unusable.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function emergency($message, array $context = array())
            {
                // TODO: Implement emergency() method.
            }

            /**
             * Action must be taken immediately.
             *
             * Example: Entire website down, database unavailable, etc. This should
             * trigger the SMS alerts and wake you up.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function alert($message, array $context = array())
            {
                // TODO: Implement alert() method.
            }

            /**
             * Critical conditions.
             *
             * Example: Application component unavailable, unexpected exception.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function critical($message, array $context = array())
            {
                // TODO: Implement critical() method.
            }

            /**
             * Runtime errors that do not require immediate action but should typically
             * be logged and monitored.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function error($message, array $context = array())
            {
                // TODO: Implement error() method.
            }

            /**
             * Exceptional occurrences that are not errors.
             *
             * Example: Use of deprecated APIs, poor use of an API, undesirable things
             * that are not necessarily wrong.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function warning($message, array $context = array())
            {
                // TODO: Implement warning() method.
            }

            /**
             * Normal but significant events.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function notice($message, array $context = array())
            {
                // TODO: Implement notice() method.
            }

            /**
             * Interesting events.
             *
             * Example: User logs in, SQL logs.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function info($message, array $context = array())
            {
                // TODO: Implement info() method.
            }

            /**
             * Detailed debug information.
             *
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function debug($message, array $context = array())
            {
                $this->_text = $message;
            }

            /**
             * Logs with an arbitrary level.
             *
             * @param mixed $level
             * @param string $message
             * @param array $context
             *
             * @return void
             */
            public function log($level, $message, array $context = array())
            {
                // TODO: Implement log() method.
            }

            public function getMessageForTest(): string
            {
                return $this->_text;
            }
        };

        $adapter = new \SuiteSoft\LaravelSms\Providers\LogAdapter($logger);
        $smsSendler = new \SuiteSoft\LaravelSms\SmsSender($adapter);
        $response = $smsSendler->send('8(926)123-45-67', 'test message');

        $this->assertEquals("0", $response->getCode());
        $this->assertEquals('Sms is sent to 89261234567: "test message"', $logger->getMessageForTest());
    }
}