<?php
declare(strict_types=1);

namespace SuiteSoft\LaravelSms;

/**
 * Class ServiceProvider
 * @package SuiteSoft\LaravelSms
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config.php' => config_path('laravel-sms.php')
        ]);
    }

    public function register()
    {
        $this->app->singleton(SmsSender::class, function ($app) {
            return new SmsSender(
                $app->make(config('laravel-sms.provider'), [
                    'options' => config('laravel-sms.provider_options')
                ])
            );
        });
    }
}
