<?php
/**
 * Created by PhpStorm.
 * User: seminsergey
 * Date: 06.08.2018
 * Time: 20:58
 */
declare(strict_types=1);

namespace SuiteSoft\LaravelSms;

use SuiteSoft\LaravelSms\Contracts\ISmsResponse;

class SmsResponse implements ISmsResponse {

    private $_messageCode;
    private $_messageId;

    public function __construct(?string $messageId, ?string $messageCode)
    {
        $this->_messageId = $messageId;
        $this->_messageCode = $messageCode;
    }

    public function getId(): string
    {
        return $this->_messageId;
    }

    public function getCode(): string
    {
        return $this->_messageCode;
    }
}