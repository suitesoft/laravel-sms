<?php
declare(strict_types=1);

namespace SuiteSoft\LaravelSms\Contracts;

/**
 * Interface BridgeInterface
 * @package App\Services\Sms\Bridges
 */
interface ISmsProvider
{
    /**
     * @param $phone
     * @param $message
     * @param array $options
     * @return ISmsResponse
     */
    public function send(string $phone, string $message, array $options = []) : ISmsResponse;

    public function status(string $messgeId): string;

}
