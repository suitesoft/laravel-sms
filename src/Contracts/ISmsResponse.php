<?php
/**
 * Created by PhpStorm.
 * User: seminsergey
 * Date: 06.08.2018
 * Time: 20:56
 */

declare(strict_types=1);

namespace SuiteSoft\LaravelSms\Contracts;


interface ISmsResponse
{
    public function getId(): string;
    public function getCode(): string;
}