<?php
declare(strict_types=1);

namespace SuiteSoft\LaravelSms;

use SuiteSoft\LaravelSms\Contracts\ISmsProvider;
use SuiteSoft\LaravelSms\Contracts\ISmsResponse;

/**
 * Class SmsSender
 * @package App\Services\Sms
 */
class SmsSender
{
    /**
     * @var ISmsProvider
     */
    private $bridge;

    /**
     * SmsSender constructor.
     * @param ISmsProvider $bridge
     */
    public function __construct(ISmsProvider $bridge)
    {
        $this->bridge = $bridge;
    }

    /**
     * @param $phone
     * @param $message
     * @param array $options
     * @return ISmsResponse
     */
    public function send($phone, $message, array $options = []): ISmsResponse
    {
        return $this->bridge->send($this->preparePhone($phone), $message, $options);
    }

    public function status(string $messageId): string
    {
        return $this->bridge->status($messageId);
    }

    private function preparePhone($phone): string
    {
        return preg_replace('/[^\d]+/', '', $phone);
    }
}
