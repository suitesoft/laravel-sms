<?php
declare(strict_types=1);


namespace SuiteSoft\LaravelSms\Providers;

use Psr\Log\LoggerInterface;
use SuiteSoft\LaravelSms\Contracts\ISmsProvider;
use SuiteSoft\LaravelSms\Contracts\ISmsResponse;
use SuiteSoft\LaravelSms\SmsResponse;

/**
 * Class Log
 * @package SuiteSoft\LaravelSms\Providers
 */
class LogAdapter implements ISmsProvider
{

    private $logWriter;

    /**
     * LogDriver constructor.
     * @param LoggerInterface $logWriter
     */
    public function __construct(LoggerInterface $logWriter)
    {
        $this->logWriter = $logWriter;
    }

    /**
     * Send single sms
     * @param $phone
     * @param $text
     * @param array $options
     * @return mixed
     */
    public function send(string $phone, string $text, array $options = []) : ISmsResponse
    {
        $this->logWriter->debug(sprintf(
            'Sms is sent to %s: "%s"',
            $phone,
            $text
        ));

        return new SmsResponse(null,"0");
    }

    public function status(string $messgeId): int
    {
       return 0;
    }
}
