<?php
declare(strict_types=1);

namespace SuiteSoft\LaravelSms\Providers;

use SuiteSoft\LaravelSms\Contracts\ISmsProvider;
use SuiteSoft\LaravelSms\Contracts\ISmsResponse;
use SuiteSoft\LaravelSms\SmsResponse;
use Zelenin\SmsRu as SmsRuApi;

/**
 * Class SmsRu
 * @package SuiteSoft\LaravelSms\Providers
 */
class SmsRuAdapter implements ISmsProvider
{
    /**
     * @var SmsRuApi\Api
     */
    private $client;

    /**
     * @var array
     */
    private $options;

    /**
     * SmsRuDriver constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * @param $phone
     * @param $text
     * @param array $options
     * @return ISmsResponse
     * @throws SmsRuApi\Exception\Exception
     */
    public function send($phone, $text, array $options = []): ISmsResponse
    {
        $response = $this->_getClient()->smsSend(
            new SmsRuApi\Entity\Sms($phone, $text)
        );

        return new SmsResponse($response->ids[0] ?? "0", $response->code);
    }


    /**
     * @return SmsRuApi\Api
     */
    private function _getClient()
    {
        if (!$this->client) {
            return $this->client = new SmsRuApi\Api(new SmsRuApi\Auth\LoginPasswordAuth(
                $this->options['login'],
                $this->options['password']
            ));
        }

        return $this->client;
    }

    public function status(string $messageId): string
    {
        $response = $this->_getClient()->smsStatus($messageId);
        return $response->code;
    }
}
